package com.baidu.statistics.utils;

import org.apache.http.Header;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by wangxing on 2018/3/23.
 */
public class Header2Map {

    public static Map<String, Object> parse(Header[] headers)
    {
        Map<String, Object> stringObjectHashMap = new HashMap<String, Object>();
        for (Header header : headers) {
            stringObjectHashMap.put(header.getName(), header.getValue());
        }
        return stringObjectHashMap;
    }
}
