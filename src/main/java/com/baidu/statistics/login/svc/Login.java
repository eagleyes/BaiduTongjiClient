package com.baidu.statistics.login.svc;

import com.alibaba.fastjson.JSON;
import com.baidu.statistics.config.Config;
import com.baidu.statistics.config.ConfigFactory;
import com.baidu.statistics.login.core.LoginConnection;
import com.baidu.statistics.login.core.LoginRequest;
import com.baidu.statistics.login.core.LoginResponse;
import com.baidu.statistics.login.core.LoginReturnCode;
import com.baidu.statistics.login.om.DoLoginRequest;
import com.baidu.statistics.login.om.DoLoginResponse;
import com.baidu.statistics.login.om.DoLogoutRequest;
import com.baidu.statistics.login.om.DoLogoutResponse;
import com.baidu.statistics.login.om.PreLoginRequest;
import com.baidu.statistics.login.om.PreLoginResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class Login {
	private static final transient Log log = LogFactory.getLog(Login.class);
	/**
	 * 预登入
	 * @return
	 * @throws Exception
	 */
	public boolean preLogin(PreLoginRequest request) throws Exception {
		LoginConnection conn = new LoginConnection();
		LoginRequest<PreLoginRequest> postData = new LoginRequest<PreLoginRequest>();
		postData.initPartDataUseConfig();
		postData.setRequest(request);
		LoginResponse<PreLoginResponse> response = conn.post(postData, PreLoginResponse.class);
		if (LoginReturnCode.OK != response.getReturnCode()) {
			log.error("[error] preLogin unsuccessfully with return code: " + response.getReturnCode());
			return false;
		}
		PreLoginResponse retData = response.getRealData();
		if (retData.getNeedAuthCode() == null) {
			log.error("[error] unexpected preLogin return data: " + JSON.toJSONString(response.getRealData()));
			return false;
		}
		if (retData.getNeedAuthCode() == true) {
			log.error("[error] preLogin return data format error: " + JSON.toJSONString(response.getRealData()));
			return false;
		}
		log.info("[notice] preLogin successfully!");
		return true;
	}
	
	/**
	 * 登入
	 * @return
	 * @throws Exception
	 */
	public LoginResponse<DoLoginResponse> doLogin() throws Exception {
		Config config = new ConfigFactory().getConfig();
		LoginConnection conn = new LoginConnection();
		LoginRequest<DoLoginRequest> postData = new LoginRequest<DoLoginRequest>();
		postData.initPartDataUseConfig();
		postData.setRequest(new DoLoginRequest(config.getString(Config.K_PASSWORD)));
		LoginResponse<DoLoginResponse> response= conn.post(postData, DoLoginResponse.class);
		return response;
	}
	
	/**
	 * 登出
	 * @param ucid 用户ID
	 * @param st 会话ID
	 * @return
	 * @throws Exception
	 */
	public boolean doLogout(DoLogoutRequest request) throws Exception {
		LoginConnection conn = new LoginConnection();
		LoginRequest<DoLogoutRequest> postData = new LoginRequest<DoLogoutRequest>();
		postData.initPartDataUseConfig();
		postData.setRequest(request);
		LoginResponse<DoLogoutResponse> response = conn.post(postData, DoLogoutResponse.class);
		if (LoginReturnCode.OK != response.getReturnCode()) {
			log.error("[error] doLogout unsuccessfully with return code: " + response.getReturnCode());
			return false;
		}
		DoLogoutResponse retData = response.getRealData();
		if (retData.getRetcode() == null) {
			log.error("[error] doLogout return data format error: " + retData);
			return false;
		}
		log.info("[notice] doLogout successfully!");
		return true;
	}
}
