package com.baidu.statistics.dataapi.constant.v20160729;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * Created by wangxing on 2018/3/7.
 * <p>
 * <p>
 * <p>
 * <p>
 * 自定义变量	custom/visit/a	custom/visit/a pv_count(浏览量(PV))
 */
public enum MethodEnum {

    网站概况_趋势数据("overview/getTimeTrendRpt",

            "{'pv_count':'浏览量(PV)'," +
                    "\"visitor_count\":\"访客数(UV)\"," +
                    "\"ip_count\":\"(IP数)\"," +
                    "\"bounce_ratio\":\"(跳出率，%)\"," +
                    "\"avg_visit_time\":\"(平均访问时长，秒)\"," +
                    "\"trans_count\":\"(转化次无数)\"," +
                    "\"contri_pv\":\"(百度推荐贡献浏览量)\"}"
    ),
    网站概况_地域分布("overview/getDistrictRpt", "{\"pv_count\":\"(浏览量(PV))\"}"),
    网站概况_通用("overview/getCommonTrackRpt", "{\"pv_count\":\"(浏览量(PV))\"}"),
    推广方式("pro/product/a", "{'show_count':'(展现量)','clk_count':'(点击量)','cost_count':'(消费，元)','ctr':'(点击率，%)','cpm':'(平均点击价格，元)','pv_count':'(浏览量(PV))','visit_count':'(访问次数)'," +
            "'visitor_count':'(访客数(UV))','new_visitor_count':'(新访客数)','new_visitor_ratio':'(新访客比率，%)','in_visit_count':'(页头访问次数)','bounce_ratio':'(跳出率，%)','avg_visit_time':'(平均访问时长，秒)'," +
            "'avg_visit_pages':'(平均访问页数)','arrival_ratio':'(抵达率)'}"),
    趋势分析("trend/time/a", "{'pv_count':'(浏览量(PV))','pv_ratio':'(浏览量占比，%)','visit_count':'(访问次数)','visitor_count':'(访客数(UV))','new_visitor_count':'(新访客数)','new_visitor_ratio':'(新访客比率，%)'," +
            "'visitorip_count':'(IP数)','bounce_ratio':'(跳出率，%)','avg_visit_time':'(平均访问时长，秒)','avg_visit_pages':'(平均访问页数)','trans_count':'(转化次数)','trans_ratio':'(转化率，%)'," +
            "'avg_trans_cost':'(平均转化成本，元)','income ':'(收益，元)','profit ':'(利润，元)','roi':'(投资回报率，%)'}"),
    百度推广趋势("pro/hour/a", "{'pv_count':'(浏览量(PV))','pv_ratio':'(浏览量占比，%)','visit_count':'(访问次数)','visitor_count':'(访客数(UV))','new_visitor_count':'(新访客数)','new_visitor_ratio':'(新访客比率，%)'," +
            "'ip_count':'(IP数)','bounce_ratio':'(跳出率，%)','avg_visit_time':'(平均访问时长，秒)','avg_visit_pages':'(平均访问页数)','trans_count':'(转化次数)','trans_ratio':'(转化率，%)'," +
            "'avg_trans_cost':'(平均转化成本，元)','income ':'(收益，元)','profit ':'(利润，元)','roi':'(投资回报率，%)'}"),
    全部来源("source/all/a", "{'pv_count':'(浏览量(PV))','pv_ratio':'(浏览量占比，%)','visit_count':'(访问次数)','visitor_count':'(访客数viewType:type(按来源类型)/site(按来源网站)','clientDevice visitor':'(UV))'," +
            "'new_visitor_count':'(新访客数)','new_visitor_ratio':'(新访客比率，%)','ip_count':'(IP数)','bounce_ratio':'(跳出率，%)','avg_visit_time':'(平均访问时长，秒)','avg_visit_pages':'(平均访问页数)'," +
            "'trans_count':'(转化次数)','trans_ratio':'(转化率，%)'}"),
    搜索引擎("source/engine/a", MethodEnum.全部来源.getMetrics().toJSONString()),
    搜索词("source/searchword/a", MethodEnum.全部来源.getMetrics().toJSONString()),
    外部链接("source/link/a", MethodEnum.全部来源.getMetrics().toJSONString()),
    受访页面("visit/toppage/a", "{'pv_count':'(浏览量(PV))','visitor_count':'(访客数(UV))','ip_count':'(IP数)','visit1_count':'(入口页次数)','outward_count':'(贡献下游浏览量)','exit_count':'(退出页次数)'," +
            "'average_stay_time':'(平均停留时长，秒)','exit_ratio':'(退出率，%)'}"),
    入口页面("visit/landingpage/a", "{'visit_count':'(访问次数)','visitor_count':'(访客数(UV))','new_visitor_count':'(新访客数)','new_visitor_ratio':'(新无访客比率，%)','ip_count':'(IP数)','out_pv_count':'(贡献浏览量)'," +
            "'bounce_ratio':'(跳出率，%)','avg_visit_time':'(平均访问时长，秒)','avg_visit_pages':'(平均访问页数)','trans_count':'(转化次数)','trans_ratio':'(转化率，%)'}"),
    受访域名("visit/topdomain/a", "{'pv_count':'(浏览量(PV))','pv_ratio':'(浏览量占比，%)','visit_count':'(访问次数)','visitor_count':'(访客数(UV))','new_visitor_count':'(新访客数)','new_visitor_ratio':'(新访客比率，%)'," +
            "'ip_count':'(IP数)','bounce_ratio':'(跳出率，%)','average_stay_time':'(平均停留时长，秒)','avg_visit_pages':'(平均访问页面)'}"),
    地域分布_按省("visit/district/a", "{'pv_count':'(浏览量(PV))', 'pv_ratio':'(浏览量占比，%)','visit_count':'(访问次数)','visitor_count':'(访客数(UV))','new_visitor_count':'(新访客数)','new_visitor_ratio':'(新访客比率，%)'," +
            "'ip_count':'(IP数)','bounce_ratio':'(跳出率，%)','avg_visit_time':'(平均访问时长，秒)','avg_visit_pages':'(平source visitor均访问页数)','trans_count':'(转化次数)','trans_ratio':'(转化率，%)'}"),
    地域分布_按国家("visit/world/a", "{'pv_count':'(浏览量(PV))', 'pv_ratio':'(浏览量占比，%)','visit_count':'(访问次数)','visitor_count':'(访客数(UV))','new_visitor_count':'(新访客数)','new_visitor_ratio':'(新访客比率，%)'," +
            "'ip_count':'(IP数)','bounce_ratio':'(跳出率，%)','avg_visit_time':'(平均访问时长，秒)','avg_visit_pages':'(平source visitor均访问页数)','trans_count':'(转化次数)','trans_ratio':'(转化率，%)'}"),
    事件跟踪("custom/event_track/a", "{'event_count':'(事件总数)','uniq_event_count':'(唯一访客事件数)','event_value':'(事件价值，元)','avg_event_value':'(平均价值，元)'}"),
    自定义变量("custom/visit/a", "{'custom/visit/a pv_count':'(浏览量(PV))','visit_count':'(访问次数)','visitor_count':'(访客数(UV))','new_visitor_count':'(新访客数)','new_visitor_ratio':'(新访客比率，%)'," +
            "'avg_visit_pages':'(平均访问页数)','avg_visit_time':'(平均访问时长，秒)','bounce_ratio':'(跳出率，%)'}");


    public String method;

    private JSONObject metrics;

    public JSONObject getMetrics() {
        return this.metrics;
    }

    private JSONObject EXParams;

    public JSONObject getEXParams() {
        return EXParams;
    }


    MethodEnum(String url, String... params) {
        this.method = url;

        if (params.length > 0) {
            this.metrics = (JSONObject) JSON.parseObject(params[0]);
            if (params.length > 1) {
                this.EXParams = (JSONObject) JSON.parseObject(params[1]);
            }
        }

    }

    public static void main(String[] args) {
        for (MethodEnum methodEnum : MethodEnum.values()) {
            System.out.println(methodEnum.metrics);
            System.out.println(methodEnum.method);
            System.out.println(methodEnum);
        }
    }


    @Override
    public String toString() {
        return super.toString();
    }
}


