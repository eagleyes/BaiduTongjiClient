package com.baidu.statistics.dataapi.om.report.v20160729;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baidu.statistics.config.Config;
import com.baidu.statistics.config.ConfigFactory;
import com.baidu.statistics.dataapi.constant.v20160729.MethodEnum;
import com.baidu.statistics.dataapi.svc.Base;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/***
 * Created by wangxing on 2018/3/29.
 * 所有null已被处理
 **/
public class QueryParam implements Serializable {
    @JSONField(ordinal = 1)
    public Header header = new Header();
    @JSONField(ordinal = 2)
    public Body body = new Body();


    private transient final Config config;

    {
        this.config = new ConfigFactory().getConfig();
        this.header.setAccountType(Integer.parseInt(config.getString(Config.K_ACCOUNT_TYPE)));
        this.header.setToken(config.getString(Config.K_TOKEN));
        this.header.setUsername(config.getString(Config.K_USERNAME));
    }

    public QueryParam(String st) {
        this.header.password = st;
    }


    public List<String> preCheck(){
        // 检查必填参数
        List<String> errorList = new ArrayList<String>();
        if (this.body.siteId == 0){
             errorList.add("body:siteId");
        }
        if (StringUtils.isEmpty(this.body.startDate)){
            errorList.add("body:startDate");
        }
        if (StringUtils.isEmpty(this.body.endDate)){
            errorList.add("body:endDate");
        }
        if (StringUtils.isEmpty(this.header.password)){
            errorList.add("header:st");
        }
        return errorList;

    }

    public String toString() {
        return JSON.toJSONString(this, SerializerFeature.WriteNullStringAsEmpty, SerializerFeature.WriteMapNullValue);
    }

    public JSONObject toJsonObject() {
        return JSONObject.parseObject(toString());
    }

    private class Header {
        @JSONField(name = "account_type")
        private int accountType;
        private String password;
        private String token;
        private String username;
        public String toString() {
            return JSON.toJSONString(this, SerializerFeature.WriteNullStringAsEmpty, SerializerFeature.WriteMapNullValue);
        }

        public JSONObject toJsonObject() {
            return JSONObject.parseObject(toString());
        }

        public int getAccountType() {
            return accountType;
        }

        public void setAccountType(int accountType) {
            this.accountType = accountType;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }
    }

    public class Body {

        @JSONField(serialize=false)
        private MethodEnum methodEnum;

        /**通常对应要查询的报告**/
        private String method;

        /**站点id**/
        @JSONField(name = "site_id")
        private int siteId;

        /**查询起始时间20160501**/
        @JSONField(name = "start_date")
        private String startDate;

        /**查询结束时间20160531**/
        @JSONField(name = "end_date")
        private String endDate;

        /**对比查询起始时间20170501**/
        @JSONField(name = "start_date2")
        private String startDate2;

        /**对比查询结束时间20170531**/
        @JSONField(name = "end_date2")
        private String endDate2;

        /**自定义指标选择，多个指标用逗号分隔**/
        private String metrics;

        /**时间粒度(只支持有该参数的报告): day/hour/week/month**/
        private String gran;

        /**指标排序，示例：visitor_count,desc**/
        private String order;

        /**获取数据偏移，用于分页；默认是0**/
        @JSONField(name = "start_index")
        private int startIndex;

        /**单次获取数据条数，用于分页；默认是20; 0表示获取所有数据**/
        @JSONField(name = "max_results")
        private int maxResults;

        @JSONField(ordinal = 99)
        public Filter filter = new Filter();

        public String toString() {
            return JSON.toJSONString(this, SerializerFeature.WriteNullStringAsEmpty, SerializerFeature.WriteMapNullValue);
        }

        public JSONObject toJsonObject() {
            return JSONObject.parseObject(toString());
        }


        //
        public class Filter implements Serializable {
            /**
            转化目标：
                    -1：全部页面目标
                    -2：全部事件目标
                    -3：时长目标
                    -4：访问页数目标**/
            private int target;

            /**来源过滤：
            through：直接访问
            search,0：搜索引擎全部
            link：外部链接**/
            private String source;

            /**设备过滤
            pc：计算机
            mobile：移动设备**/
            private String clientDevice;

            /**地域过滤
            china：全国
            province,1：省市自治区北京
            province,2：省市自治区上海
            province,3：省市自治区天津
                    …
            other：其他**/
            private String area;

            /**访客过滤
            new：新访客
            old：老访客**/
            private String visitor;

            /**过滤标志位：
             * 推广方式专用——flag：product
             * 事件跟踪专用——flag：category(类别)/action(操作)/label(标签)
             * 自定义变量专用——flag: index1(访客类型1)/index2(访客类型2)/index3(访客类型3)/index4(访客类型4)/index5(访客类型5)**/
            private String flag;

            /**
             * 全部来源专用——viewType:type(按来源类型)/site(按来源网站)
             * 外部链接专用——viewType:domain(按域名)/url(按URL)
             */
            private String viewType;

            /**
             * 外部链接专用——domainType:1(社会化媒体)/2(导航网站)/4(电子邮箱)
             */
            private int domainType;

            public String toString() {
                return JSON.toJSONString(this, SerializerFeature.WriteNullStringAsEmpty, SerializerFeature.WriteMapNullValue);
            }

            public JSONObject toJsonObject() {
                return (JSONObject) JSON.toJSON(this);
            }

            public int getTarget() {
                return target;
            }

            public void setTarget(int target) {
                this.target = target;
            }

            public String getSource() {
                return source;
            }

            public void setSource(String source) {
                this.source = source;
            }

            public String getClientDevice() {
                return clientDevice;
            }

            public void setClientDevice(String clientDevice) {
                this.clientDevice = clientDevice;
            }

            public String getArea() {
                return area;
            }

            public void setArea(String area) {
                this.area = area;
            }

            public String getVisitor() {
                return visitor;
            }

            public void setVisitor(String visitor) {
                this.visitor = visitor;
            }

            public String getFlag() { return flag; }

            public void setFlag(String flag) { this.flag = flag; }

            public String getViewType() { return viewType; }

            public void setViewType(String viewType) { this.viewType = viewType; }

            public int getDomainType() { return domainType; }

            public void setDomainType(int domainType) { this.domainType = domainType; }
        }

        public MethodEnum getMethodEnum() {
            return methodEnum;
        }

        public void setMethodEnum(MethodEnum methodEnum) {
            this.method = methodEnum.method;
            this.methodEnum = methodEnum;
        }

        public String getMethod() {
            return method;
        }

        private void setMethod(String method) {
            this.method = method;
        }

        public int getSiteId() {
            return siteId;
        }

        public void setSiteId(int siteId) {
            this.siteId = siteId;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public String getStartDate2() {
            return startDate2;
        }

        public void setStartDate2(String startDate2) {
            this.startDate2 = startDate2;
        }

        public String getEndDate2() {
            return endDate2;
        }

        public void setEndDate2(String endDate2) {
            this.endDate2 = endDate2;
        }

        public String getMetrics() {
            return metrics;
        }

        public void setMetrics(String metrics) {
            this.metrics = metrics;
        }

        public String getGran() {
            return gran;
        }

        public void setGran(String gran) {
            this.gran = gran;
        }

        public String getOrder() {
            return order;
        }

        public void setOrder(String order) {
            this.order = order;
        }

        public int getStartIndex() {
            return startIndex;
        }

        public void setStartIndex(int startIndex) {
            this.startIndex = startIndex;
        }

        public int getMaxResults() {
            return maxResults;
        }

        public void setMaxResults(int maxResults) {
            this.maxResults = maxResults;
        }

    }
}
