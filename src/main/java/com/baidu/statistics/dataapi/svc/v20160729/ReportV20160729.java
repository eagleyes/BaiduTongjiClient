package com.baidu.statistics.dataapi.svc.v20160729;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baidu.statistics.config.Config;
import com.baidu.statistics.dataapi.constant.v20160729.MethodEnum;
import com.baidu.statistics.dataapi.om.report.v20160729.QueryParam;
import com.baidu.statistics.dataapi.svc.Base;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by wangxing on 2018/3/28.
 */
public class ReportV20160729 extends Base {
    ResponseHandler handler;
    Then<Map> then;
    public ReportV20160729(Integer ucid, String st) {
        super(ucid, st);
        handler = new ResponseHandler() {
            public JSONObject handleResponse(HttpResponse response)
                    throws IOException {
                if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                    then.onError(response.getStatusLine().getStatusCode(), "响应报文状态码异常");
                }
                HttpEntity entity = response.getEntity();
                String retStr = EntityUtils.toString(entity, "UTF-8");

                JSONObject retJson = JSON.parseObject(retStr);
                JSONObject bodyJson = retJson.getJSONObject("body");
                if (then.assertSuccess(retJson)) {
                    then.onSuccess(bodyJson);
                } else {
                    then.onFail(retJson);
                }
                return retJson;
            }
        };
    }

    /**
     * 获取网站列表
     * @param then
     */
    public JSONObject getSites(final Then<Map> then){
        Map<String, String> header = new HashMap<String, String>();


        //UUID 必填，唯一标识符，与登录时一致
        header.put("UUID", config.getString(Config.K_UUID)); //必填，唯一标识符，与登录时一致
        //USERID 必填，成功登录后获取的用户id（ucid）
        header.put("USERID", String.valueOf(this.ucid)); //必填，成功登录后获取的用户id（ ucid）
        //tracker选填，请求定位符，可用于定位请求
        header.put("tracker", ""); //选填，请求定位符，可用于定位请求
        header.put("Content-Type", "data/json;charset=UTF-8");

        JSONObject data = JSON.parseObject("{\"body\":{\"methodName\":\"getsites\",\"serviceName\":\"method\"},\"header\":{\"account_type\":1,\"password\":\""+this.st+"\",\"token\":\"0ca22f9f98cf5b6839f08353e696143f\",\"username\":\"markor\"}}");

        sendPost(this.GET_SITES_URL, header, data, handler, then);
        return (JSONObject) then.result;
    }

    /**
     * 根据不同method拉取相应统计报表
     * @param methodEnum  报表接口方法枚举
     * @param data  QueryParam实体
     * @param then  回调事件
     */
    public void getReport(MethodEnum methodEnum, QueryParam data, final Then<Map> then) {

        if (methodEnum==null||data==null||then==null){
            then.onError("调用此方法必须确保3个有效入参");
            return;
        }
        Map<String, String> header = new HashMap<String, String>();


        //UUID 必填，唯一标识符，与登录时一致
        header.put("UUID", config.getString(Config.K_UUID)); //必填，唯一标识符，与登录时一致
        //USERID 必填，成功登录后获取的用户id（ucid）
        header.put("USERID", String.valueOf(this.ucid)); //必填，成功登录后获取的用户id（ ucid）
        //tracker选填，请求定位符，可用于定位请求
        header.put("tracker", ""); //选填，请求定位符，可用于定位请求
        header.put("Content-Type", "data/json;charset=UTF-8");

        if (data.preCheck().size() > 0){
            then.onError("缺少必需参数：" + StringUtils.join(data.preCheck(), ","));
            return;
        }

        data.body.setMethodEnum(methodEnum);

        if (StringUtils.isEmpty(data.body.getMetrics())){
            data.body.setMetrics(StringUtils.join(methodEnum.getMetrics().keySet().toArray(), ",").replace("contri_pv,", ""));
        }

        sendPost(this.REPORT_URL, header, data.toJsonObject(), handler, then);


    }

    private void sendPost(String url, Map<String, String> header, Map<String, Object> data, ResponseHandler rh, final Then<Map> then){
        this.then = then;
        CloseableHttpClient client = HttpClients.createMinimal();
        HttpPost post_request = new HttpPost(url);
        JSONObject result = null;
        for (String key : header.keySet()) {
            post_request.addHeader(key, header.get(key));
        }

        post_request.setEntity(new StringEntity(JSON.toJSONString(data), "UTF-8"));

        try {
            then.onStart(data);
            result = (JSONObject) client.execute(post_request, rh);
        } catch (ClientProtocolException e) {
            then.onError(e.getMessage());
        } catch (IOException e) {
            then.onError(e.getMessage());
        } finally {
            then.onFinished(data, result);
        }


    }


}
