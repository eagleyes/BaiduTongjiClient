package com.baidu.statistics.dataapi.svc;

import com.alibaba.fastjson.JSONObject;
import com.baidu.statistics.config.Config;
import com.baidu.statistics.config.ConfigFactory;

import java.util.HashMap;
import java.util.Map;

public abstract class Base {
	protected String GET_SITES_URL;
	protected String REPORT_URL;
	protected Config config;
	protected Integer ucid;
	protected String st;

	public Base(Integer ucid, String st) {
		this.ucid = ucid;
		this.st = st;
		this.config = new ConfigFactory().getConfig();
		this.REPORT_URL = this.config.getString("report_api_url_v20160729");
		this.GET_SITES_URL = this.config.getString("get_siteList_url_v20160729");
	}
	
	public Integer getUcid() {
		return ucid;
	}
	public String getSt() {
		return st;
	}
}
