package com.baidu.statistics.dataapi.svc.v20160729;

import javafx.util.Callback;

import java.util.Map;

/**
 * Created by wangxing on 2018/3/28.
 */
public abstract class Then<T> implements Callback {
    public Object call(Object param) {
        return null;
    }

    public Object result;

    public abstract boolean assertSuccess(T obj);

    public abstract void onStart(T request);
    public abstract void onProcess();
    public abstract void onSuccess(T response);
    public abstract void onFail(T response);
    public abstract void onError(int code, String msg);
    public abstract void onError(String msg);
    public void onFinished(T request, T response){
        this.result = response;
    }
}