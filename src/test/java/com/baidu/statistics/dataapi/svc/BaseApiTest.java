package com.baidu.statistics.dataapi.svc;

import com.baidu.statistics.dataapi.svc.v20160729.ReportV20160729;
import com.baidu.statistics.login.om.DoLoginResponse;
import com.baidu.statistics.login.svc.BaseLoginTest;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;

public class BaseApiTest {

	private static Integer ucid;
	private static String st;
	
	private static BaseLoginTest login;
	
	protected static ReportV20160729 reportV20160729;

	static boolean DEBUG = true;
	
	@BeforeClass
	public static void login() throws Exception {
		login = new BaseLoginTest();
		
		DoLoginResponse retData = login.doLogin();
		Assert.assertNotNull(retData);
		
		ucid = retData.getUcid();
		st = retData.getSt();
		reportV20160729 = new ReportV20160729(ucid, st);
	}
	
	@AfterClass
	public static void logout() throws Exception {

		boolean ret = true?DEBUG:login.doLogout(ucid, st);
		Assert.assertSame(ret, true);
	}
	

}
