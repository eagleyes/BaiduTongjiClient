package com.baidu.statistics.dataapi.svc.v20160729;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baidu.statistics.dataapi.constant.v20160729.MethodEnum;
import com.baidu.statistics.dataapi.om.report.v20160729.QueryParam;
import com.baidu.statistics.dataapi.svc.BaseApiTest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;

import java.util.Map;

/**
 * Created by wangxing on 2018/3/28.
 */
public class ReportTestV20160729 extends BaseApiTest {
    private static final transient Log log = LogFactory.getLog(ReportTestV20160729.class);
    private JSONObject sites;
    private Then<Map> then = new Then<Map>(){
        public boolean assertSuccess(Map obj) {
            return "success".equals((((JSONObject)obj).getJSONObject("header")).getString("desc"));
        }

        public void onStart(Map request) {
            log.debug("\n开始发送请求："+ JSON.toJSONString(request));
        }

        public void onProcess() {
        }

        public void onSuccess(Map response) {
            log.debug("\n请求成功:"+JSON.toJSONString(response));
        }

        public void onFail(Map response) {
            log.debug("\n请求失败:"+JSON.toJSONString(response));
        }

        public void onError(int code, String msg) {
            log.debug("\n请求失败:"+msg+"code:" + code);
        }

        public void onError(String msg) {
            log.debug("\n请求失败:"+msg);
        }

        public void onFinished(Map request) {
            log.debug("请求结束");
        }

    };

    @Test
    public void gitSitesList() throws Exception {

        reportV20160729.getSites(then);
        sites = (JSONObject) then.result;
    }

    /**网站概况_趋势数据**/
    @Test
    public void getReportA() throws Exception {
        gitSitesList();
        QueryParam param = new QueryParam(reportV20160729.getSt());
        param.body.setSiteId(sites.getJSONObject("body").getJSONArray("data").getJSONObject(0).getJSONArray("list").getJSONObject(0).getIntValue("site_id"));
        param.body.setStartDate("20170101");
        param.body.setEndDate("20170301");
        reportV20160729.getReport(MethodEnum.网站概况_趋势数据, param, then);
    }
    /**推广方式**/
    @Test
    public void getReportB() throws Exception {
        gitSitesList();
        QueryParam param = new QueryParam(reportV20160729.getSt());
        param.body.setSiteId(sites.getJSONObject("body").getJSONArray("data").getJSONObject(0).getJSONArray("list").getJSONObject(0).getIntValue("site_id"));
        param.body.setStartDate("20170101");
        param.body.setEndDate("20170301");
        param.body.filter.setFlag("product");
        reportV20160729.getReport(MethodEnum.推广方式, param, then);
    }
}
