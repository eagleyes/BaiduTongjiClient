# 百度统计数据拉取工具包statistic

```
com.baidu.statistic:statistic-0.0.1-SNAPSHOT.jar
```

该工具已被封装成jar包，目前美克没有自己的maven仓库，所以需要此jar包请线下索取。



## JAR工具包  API

### 登录  

com.baidu.statistics.login.svc.Login

| method   | parameters                                                   | return                                               | description                 |
| -------- | ------------------------------------------------------------ | ---------------------------------------------------- | --------------------------- |
| preLogin | com.baidu.statistics.login.om.PreLoginRequest（请求体对象，下文介绍） | boolean                                              | 登录前检查,检查通过方可登录 |
| doLogin  |                                                              | DoLoginResponse，其中"st","ucid"属性用以后续接口鉴权 | 登录                        |
| doLogout | com.baidu.statistics.login.om.DoLogoutRequest（请求体对象，下文介绍） | boolean                                              | 注销                        |

#### Entity:  com.baidu.statistics.login.om.PreLoginRequest

| parameter     | desc               | example                              |
| ------------- | ------------------ | ------------------------------------ |
| osVersion     | 客户端载体操作系统 | StatisticConstant.OS_VERSION_WINDOWS |
| deviceType    | 客户端载体类型     | StatisticConstant.DEVICE_TYPE_PC     |
| clientVersion | 客户端版本         | "1.0"                                |

#### Entity: com.baidu.statistics.login.om.DoLogoutRequest

| parameter | desc                        |
| --------- | --------------------------- |
| ucid      | 登录时返回的客户id          |
| st        | 登录时返回的st（会话token） |

### 获取网站列表及报表数据

com.baidu.statistics.dataapi.svc.v20160729.ReportV20160729

| method      | parameters                                                   | return                         | desc                 |
| ----------- | ------------------------------------------------------------ | ------------------------------ | -------------------- |
| constructor | Integer ucid, String st                                      | self                           | 构造方法             |
| getSites    | com.baidu.statistics.dataapi.svc.v20160729.Then<Map>回调处理 | JSONObject详见getSites-return  | 返回百度统计站点列表 |
| getReport   | com.baidu.statistics.dataapi.constant.v20160729.MethodEnum<br>com.baidu.statistics.dataapi.om.report.v20160729.QueryParam<br>com.baidu.statistics.dataapi.svc.v20160729.Then<Map> | JSONObject详见getReport-return | 返回报表数据         |

#### getSites-return

```json
{
    "header": {
        "desc": "success",
        "failures": [],
        "oprs": 1,
        "succ": 1,
        "oprtime": 0,
        "quota": 1,
        "rquota": 49997,
        "status": 0
    },
    "body": {
        "data": [
            {
                "list": [
                    {
                        "status": 0,
                        "create_time": "2013-01-15 18:48:02",
                        "domain": "markorhome.com",
                        "site_id": 2260651,
                        "sub_dir_list": []
                    },
                    {
                        "status": 0,
                        "create_time": "2015-01-08 15:38:01",
                        "domain": "121.199.12.77",
                        "site_id": 6151842,
                        "sub_dir_list": []
                    },
                    {
                        "status": 0,
                        "create_time": "2015-01-08 16:24:38",
                        "domain": "60.30.28.30:8090",
                        "site_id": 6152441,
                        "sub_dir_list": []
                    },
                    {
                        "status": 0,
                        "create_time": "2015-09-29 10:46:29",
                        "domain": "www.markorhome.com/about-actions",
                        "site_id": 7509142,
                        "sub_dir_list": []
                    },
                    {
                        "status": 0,
                        "create_time": "2017-07-29 19:52:27",
                        "domain": "artepicenters.com",
                        "site_id": 11012475,
                        "sub_dir_list": []
                    },
                    {
                        "status": 0,
                        "create_time": "2018-03-19 12:34:39",
                        "domain": "art.rehome.humming-tech.com",
                        "site_id": 11795242,
                        "sub_dir_list": []
                    },
                    {
                        "status": 0,
                        "create_time": "2018-03-19 12:37:19",
                        "domain": "mkhome.rehome.humming-tech.com",
                        "site_id": 11795244,
                        "sub_dir_list": []
                    }
                ]
            }
        ]
    }
}
```

#### getReport-return

```json
{
    "header": {
        "desc": "success",
        "failures": [],
        "oprs": 1,
        "succ": 1,
        "oprtime": 0,
        "quota": 1,
        "rquota": 49987,
        "status": 0
    },
    "body": {
        "data": [
            {
                "result": {
                    "items": [
                        [
                            [
                                "2018/03/22与2018/02/22"
                            ],
                            [
                                "2018/03/23与2018/02/23"
                            ]
                        ],
                        [
                            [
                                "2018/03/22",
                                949,
                                186,
                                176,
                                39.06,
                                403,
                                "--"
                            ],
                            [
                                "2018/03/23",
                                1911,
                                560,
                                536,
                                62.63,
                                297,
                                "--"
                            ]
                        ],
                        [
                            [
                                "2018/02/22",
                                97,
                                39,
                                38,
                                46.34,
                                210,
                                "--"
                            ],
                            [
                                "2018/02/23",
                                126,
                                46,
                                45,
                                45.83,
                                189,
                                "--"
                            ]
                        ],
                        []
                    ],
                    "timeSpan": [
                        "2018/03/22 - 2018/03/23",
                        "2018/02/22 - 2018/02/23"
                    ],
                    "fields": [
                        "simple_date_title",
                        "time",
                        "pv_count",
                        "visitor_count",
                        "ip_count",
                        "bounce_ratio",
                        "avg_visit_time",
                        "trans_count"
                    ]
                }
            }
        ]
    }
}
```



## http请求 api

### getSiteList

作用：获取当前用户下的站点和子目录列表以及对应参数信息，不包括权限站点和汇总网站。

您需要使用正确的站点ID（网站或系统中安装了哪个站点的代码，就选择哪个站点）后，方可使用getData服务导出正确的报告数据。

#### 接口名称

<https://api.baidu.com/json/tongji/v1/ReportService/getSiteList>

#### 返回参数

| 参数名 | 参数类型          | 是否必需 | 描述     |
| ------ | ----------------- | -------- | -------- |
| list   | array of SiteInfo | 是       | 站点列表 |

\1. SiteInfo结构

| 参数名       | 参数类型            | 是否必需 | 描述                         |
| ------------ | ------------------- | -------- | ---------------------------- |
| site_id      | uint                | 是       | 应用ID                       |
| domain       | string              | 是       | 站点域名                     |
| status       | uint                | 是       | 0：正常；1：暂停             |
| create_time  | DateTime            | 是       | 日期时间格式，以北京时间表示 |
| sub_dir_list | array of SubDirInfo | 是       | 子目录列表                   |

\2. SubDirInfo结构

| 参数名       | 参数类型            | 是否必需 | 描述                         |
| ------------ | ------------------- | -------- | ---------------------------- |
| sub_dir_id   | uint                | 是       | 子目录ID                     |
| name         | string              | 是       | 子目录名称                   |
| status       | uint                | 是       | 0：正常；1：暂停             |
| create_time  | DateTime            | 是       | 日期时间格式，以北京时间表示 |
| sub_dir_list | array of SubDirInfo | 是       | 子目录列表                   |

##### 返回示例

```json
{
    "header": {
        "desc": "success",
        "failures": [

        ],
        "oprs": 1,
        "succ": 1,
        "oprtime": 1,
        "quota": 1,
        "rquota": 1538,
        "status": 0
    },
    "body": {
        "data": [
            {
                "list": [
                    {
                        "site_id": 12345,
                        "domain": "www.iqiyi.com",
                        "status": 0,
                        "create_time": "2011-01-01 15:00:00",
                        "sub_dir_list": [
                            {
                                "sub_dir_id": 123456,
                                "name": "视频",
                                "create_time": "2011-01-02 15:00:00"
                            },
                            {
                                "sub_dir_id": 123457,
                                "name": "我的积分",
                                "create_time": "2011-01-03 15:00:00"
                            }
                        ]
                    },
                    {
                        "site_id": 123458,
                        "domain": "www.iqiyi.com/life",
                        "status": 0,
                        "create_time": "2011-01-04 15:00:00",
                        "sub_dir_list": [

                        ]
                    }
                ]
            }
        ]
    }
}
        
```



### getData

作用：根据站点ID获取站点报告数据。

确认所需的站点ID后，根据您的数据分析需求指定报告、指标、筛选维度，使用getData获取数据即可。

#### 接口名称

<https://api.baidu.com/json/tongji/v1/ReportService/getData>

#### 输入参数

\1. 基本参数

| 参数名      | 参数类型 | 是否必需 | 描述                                                    |
| ----------- | -------- | -------- | ------------------------------------------------------- |
| site_id     | uint     | 是       | 站点id                                                  |
| method      | string   | 是       | 通常对应要查询的报告                                    |
| start_date  | string   | 是       | 查询起始时间,例：20160501                               |
| end_date    | string   | 是       | 查询结束时间,例：20160531                               |
| start_date2 | string   | 否       | 对比查询起始时间                                        |
| end_date2   | string   | 否       | 对比查询结束时间                                        |
| metrics     | string   | 是       | 自定义指标选择，多个指标用逗号分隔                      |
| gran        | string   | 否       | 时间粒度(只支持有该参数的报告): day/hour/week/month     |
| order       | string   | 否       | 指标排序，示例：visitor_count,desc                      |
| start_index | uint     | 否       | 获取数据偏移，用于分页；默认是0                         |
| max_results | uint     | 否       | 单次获取数据条数，用于分页；默认是20; 0表示获取所有数据 |

注:

- **method参数**，通常对应要查询的报告，与网站页面所发送请求的method参数一致，如要获取趋势分析报告的数据，所用到的方法为“trend/time/a”。
- **metrics参数**，是所要获取的指标，根据不同的报告填写相关指标，与网站页面所发送请求的indicators参数一致，如要获取浏览量(PV)、访客数(UV)、新访客数，则所填指标为“pv_count,visitor_count,new_visitor_count”。

\2. 筛选参数

| 参数名       | 参数类型 | 是否必需 | 描述                                                         |
| ------------ | -------- | -------- | ------------------------------------------------------------ |
| target       | int      | 否       | 转化目标：-1：全部页面目标-2：全部事件目标-3：时长目标-4：访问页数目标 |
| source       | string   | 否       | 来源过滤：through：直接访问search,0：搜索引擎全部link：外部链接 |
| clientDevice | string   | 否       | 设备过滤pc：计算机mobile：移动设备                           |
| area         | string   | 否       | 地域过滤china：全国province,1：省市自治区北京province,2：省市自治区上海province,3：省市自治区天津…other：其他 |
| visitor      | string   | 否       | 访客过滤new：新访客old：老访客                               |

#### 关键参数与报告的对应

| 报告         | method                                                       | metrics(指标，数据单位)                                      | 其他参数                                                     |
| ------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 网站概况     | overview/getTimeTrendRpt (趋势数据)                          | 单选： pv_count(浏览量(PV))visitor_count(访客数(UV))ip_count(IP数)bounce_ratio(跳出率，%)avg_visit_time(平均访问时长，秒)trans_count(转化次无数)contri_pv(百度推荐贡献浏览量) | 无                                                           |
|              | overview/getDistrictRpt (地域分布)                           | pv_count(浏览量(PV))                                         | 无                                                           |
|              | overview/getCommonTrackRpt (来源网站、搜索词、入口页面、受访页面) | pv_count(浏览量(PV))                                         | 无                                                           |
| 推广方式     | pro/product/a                                                | show_count(展现量)clk_count(点击量)cost_count(消费，元)ctr(点击率，%)cpm(平均点击价格，元)pv_count(浏览量(PV))visit_count(访问次数)visitor_count(访客数(UV))new_visitor_count(新访客数)new_visitor_ratio(新访客比率，%)in_visit_count(页头访问次数)bounce_ratio(跳出率，%)avg_visit_time(平均访问时长，秒)avg_visit_pages(平均访问页数)arrival_ratio(抵达率) | flag:productarea                                             |
| 趋势分析     | trend/time/a                                                 | pv_count(浏览量(PV))pv_ratio(浏览量占比，%)visit_count(访问次数)visitor_count(访客数(UV))new_visitor_count(新访客数)new_visitor_ratio(新访客比率，%)visitorip_count(IP数)bounce_ratio(跳出率，%)avg_visit_time(平均访问时长，秒)avg_visit_pages(平均访问页数)trans_count(转化次数)trans_ratio(转化率，%)avg_trans_cost(平均转化成本，元)income (收益，元)profit (利润，元)roi(投资回报率，%) | sourceclientDeviceareavisitor                                |
| 百度推广趋势 | pro/hour/a                                                   | pv_count(浏览量(PV))pv_ratio(浏览量占比，%)visit_count(访问次数)visitor_count(访客数(UV))new_visitor_count(新访客数)new_visitor_ratio(新访客比率，%)ip_count(IP数)bounce_ratio(跳出率，%)avg_visit_time(平均访问时长，秒)avg_visit_pages(平均访问页数)trans_count(转化次数)trans_ratio(转化率，%)avg_trans_cost(平均转化成本，元)income (收益，元)profit (利润，元)roi(投资回报率，%) | clientDevicearea                                             |
| 全部来源     | source/all/a                                                 | pv_count(浏览量(PV))pv_ratio(浏览量占比，%)visit_count(访问次数)visitor_count(访客数viewType:type(按来源类型)/site(按来源网站)clientDevice visitor(UV))new_visitor_count(新访客数)new_visitor_ratio(新访客比率，%)ip_count(IP数)bounce_ratio(跳出率，%)avg_visit_time(平均访问时长，秒)avg_visit_pages(平均访问页数)trans_count(转化次数)trans_ratio(转化率，%) | viewType:type(按来源类型)/site(按来源网站)clientDevicevisitor |
| 搜索引擎     | source/engine/a                                              | 同上                                                         | clientDeviceareavisitor                                      |
| 搜索词       | source/searchword/a                                          | 同上                                                         | sourceclientDevicevisitor                                    |
| 外部链接     | source/link/a                                                | 同上                                                         | viewType:domain(按域名)/url(按URL)domainType:1(社会化媒体)/2(导航网站)/4(电子邮箱)clientDevicevisitor |
| 受访页面     | visit/toppage/a                                              | pv_count(浏览量(PV))visitor_count(访客数(UV))ip_count(IP数)visit1_count(入口页次数)outward_count(贡献下游浏览量)exit_count(退出页次数)average_stay_time(平均停留时长，秒)exit_ratio(退出率，%) | sourcevisitor                                                |
| 入口页面     | visit/landingpage/a                                          | visit_count(访问次数)visitor_count(访客数(UV))new_visitor_count(新访客数)new_visitor_ratio(新无访客比率，%)ip_count(IP数)out_pv_count(贡献浏览量)bounce_ratio(跳出率，%)avg_visit_time(平均访问时长，秒)avg_visit_pages(平均访问页数)trans_count(转化次数)trans_ratio(转化率，%) | 无                                                           |
| 受访域名     | visit/topdomain/a                                            | pv_count(浏览量(PV))pv_ratio(浏览量占比，%)visit_count(访问次数)visitor_count(访客数(UV))new_visitor_count(新访客数)new_visitor_ratio(新访客比率，%)ip_count(IP数)bounce_ratio(跳出率，%)average_stay_time(平均停留时长，秒)avg_visit_pages(平 | sourcevisitor                                                |
| 地域分布     | visit/district/a (按省)                                      | pv_count(浏览量(PV)) pv_ratio(浏览量占比，%)visit_count(访问次数)visitor_count(访客数(UV))new_visitor_count(新访客数)new_visitor_ratio(新访客比率，%)ip_count(IP数)bounce_ratio(跳出率，%)avg_visit_time(平均访问时长，秒)avg_visit_pages(平source visitor均访问页数)trans_count(转化次数)trans_ratio(转化率，%) | sourcevisitor                                                |
|              | visit/world/a (按国家)                                       | 同上                                                         | 同上                                                         |
| 事件跟踪     | custom/event_track/a                                         | event_count(事件总数)uniq_event_count(唯一访客事件数)event_value(事件价值，元)avg_event_value(平均价值，元) | flag:category(类别)/action(操作)/label(标签)                 |
| 自定义变量   | custom/visit/a                                               | custom/visit/a pv_count(浏览量(PV))visit_count(访问次数)visitor_count(访客数(UV))new_visitor_count(新访客数)new_visitor_ratio(新访客比率，%)avg_visit_pages(平均访问页数)avg_visit_time(平均访问时长，秒)bounce_ratio(跳出率，%) | flag:index1(访客类型1)/index2(访客类型2)/index3(访客类型3)/index4(访客类型4)/index5(访客类型5) |

#### 请求示例

这里通过列举若干调用实例来进一步阐述该接口的使用方式，其中会根据获取数据的不同而传入不同的请求参数。

\1. 获取趋势分析报告中PV和UV数据，按天粒度

| 参数        | 值                     | 描述                 |
| ----------- | ---------------------- | -------------------- |
| site_id     | SITE_ID                | 站点ID               |
| method      | trend/time/a           | 趋势分析报告         |
| start_date  | 20160501               | 所查询数据的起始日期 |
| end_date    | 20160531               | 所查询数据的结束日期 |
| metrics     | pv_count,visitor_count | 所查询指标为PV和UV   |
| max_results | 0                      | 返回所有条数         |
| gran        | day                    | 按天粒度             |

\2. 获取地域分布报告中的跳出率、平均访问时长、平均访问页数指标，按平均访问页数指标排序（倒序），并做新访客的筛选

| 参数       | 值                                          | 描述                                           |
| ---------- | ------------------------------------------- | ---------------------------------------------- |
| site_id    | SITE_ID                                     | 站点ID                                         |
| method     | visit/district/a                            | 地域分布报告                                   |
| start_date | 20160501                                    | 所查询数据的起始日期                           |
| end_date   | 20160531                                    | 所查询数据的结束日期                           |
| metrics    | bounce_ratio,avg_visit_time,avg_visit_pages | 所查询指标为跳出率、平均访问时长、平均访问页数 |
| visitor    | new                                         | 新访客筛选                                     |
| order      | avg_visit_pages,desc                        | 按平均访问页数指标排序（倒序）                 |

#### 返回参数

| 参数名 | 参数类型   | 是否必需 | 描述     |
| ------ | ---------- | -------- | -------- |
| result | ReportData | 是       | 报告数据 |

\1. ReportData结构

| 参数名 | 参数类型        | 是否必需 | 描述                                                         |
| ------ | --------------- | -------- | ------------------------------------------------------------ |
| fields | array of string | 是       | 指标列表                                                     |
| sum    | array of string | 是       | 总计数据                                                     |
| items  | array           | 是       | 指标数据，有4部分构成：0维度数据1指标数据2对比时间段数据3变化率数据 |
| total  | uint            | 是       | 总计条目                                                     |

#### 返回示例

```
{
    "header": {
        "desc": "success",
        "failures": [

        ],
        "oprs": 1,
        "succ": 1,
        "oprtime": 1,
        "quota": 1,
        "rquota": 1538,
        "status": 0
    },
    "body": {
        "data": [
            {
                "result": {
                    "offset": 0,
                    "timeSpan": [
                        "2016/05/31"
                    ],
                    "fields": [
                        "visit_district_title",
                        "pv_count",
                        "visitor_count",
                        "ip_count"
                    ],
                    "total": 35,
                    "sum": [
                        [
                            88498794,
                            35978529,
                            25665169
                        ],
                        [

                        ]
                    ],
                    "pageSum": [
                        [
                            35405459,
                            14085496,
                            9982100
                        ],
                        [

                        ],
                        [

                        ]
                    ],
                    "items": [
                        [
                            [
                                {
                                    "name": "广东",
                                    "area": "province,4"
                                }
                            ],
                            [
                                {
                                    "name": "浙江",
                                    "area": "province,32"
                                }
                            ],
                            [
                                {
                                    "name": "其他",
                                    "area": "province,0"
                                }
                            ],
                            [
                                {
                                    "name": "江苏",
                                    "area": "province,19"
                                }
                            ],
                            [
                                {
                                    "name": "河南",
                                    "area": "province,14"
                                }
                            ]
                        ],
                        [
                            [
                                12341436,
                                4761366,
                                3272131
                            ],
                            [
                                6246400,
                                2439809,
                                1818061
                            ],
                            [
                                5843763,
                                2371240,
                                1583733
                            ],
                            [
                                5786661,
                                2349302,
                                1784306
                            ],
                            [
                                5187199,
                                2163779,
                                1523869
                            ]
                        ],
                        [

                        ],
                        [

                        ]
                    ]
                }
            }
        ]
    }
}
        
```